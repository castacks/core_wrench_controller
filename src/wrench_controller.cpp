#include <wrench_controller/wrench_controller.h>
#include <cmath>
#include <queue>
#include <utility>
#include <vector>

#define IGNORE_TORQUE
// #define DEBUG

WrenchController::WrenchController(const std::string &sensor_frame, 
    const std::string &robot_frame, const std::string &world_frame, 
    const std::string &thrust_output_frame, const std::string &torque_output_frame, 
    double hover_thrust, bool filter_ft_data) :
    _sensor_frame_str(sensor_frame), _robot_frame_str(robot_frame), _world_frame_str(world_frame), 
    _thrust_output_frame_str(thrust_output_frame), _torque_output_frame_str(torque_output_frame),
    _hover_thrust(hover_thrust), _filter_ft_data(filter_ft_data), _last_thrust(0.f), 
    _got_ft_data(false), _got_target_wrench(false)
{
  _fx_controller = new PIDController("~/fx");
  _fy_controller = new PIDController("~/fy");
  _fz_controller = new PIDController("~/fz");
  #ifndef IGNORE_TORQUE
    _tx_controller = new PIDController("~/tx");
    _ty_controller = new PIDController("~/ty");
    _tz_controller = new PIDController("~/tz");
  #endif
}

void WrenchController::UpdateTarget(const geometry_msgs::WrenchStamped &target, const tf::TransformListener* tf_listener)
{
  // Transform the target wrench to the sensor frame
  tf::StampedTransform target_to_sensor_tf;
  try
  {
    _target_wrench = target;
    // Get transform from the target wrench to sensor coordinates
    tf_listener->waitForTransform(_sensor_frame_str, target.header.frame_id,
        target.header.stamp, ros::Duration(0.1));
    tf_listener->lookupTransform(_sensor_frame_str, target.header.frame_id,
        target.header.stamp, target_to_sensor_tf);

    // Transform forces to sensor frame
    _target_force_sensor_frame = target_to_sensor_tf * tf::Vector3(
        target.wrench.force.x,
        target.wrench.force.y,
        target.wrench.force.z);

    #ifndef IGNORE_TORQUE
      // Transform moments to sensor frame
      _target_torque_sensor_frame = target_to_sensor_tf * tf::Vector3(
          target.wrench.torque.x,
          target.wrench.torque.y,
          target.wrench.torque.z);
    #endif

    if (_got_target_wrench == false)
    {
      ROS_INFO("Successfully received the desired wrench. This message will not be published again.");
      _got_target_wrench = true;
    }
  }
  catch (tf::TransformException ex)
  {
    ROS_ERROR_THROTTLE(3, "Desired wrench TF lookup failed: %s", ex.what());
  }
}

geometry_msgs::WrenchStamped WrenchController::UpdateState(const geometry_msgs::WrenchStamped &ft_data, const tf::TransformListener *tf_listener)
{
  // Transform the measured f/t sensor data to 
  tf::StampedTransform ftdata_to_sensor_tf;
  try
  {
    _current_wrench = ft_data;
    
    // Get the transform for the measured data to the sensor frame
    // Note that we may have a different coordinates for the measured data than what we have here, 
    // so this step is necessary.
    tf_listener->waitForTransform(_sensor_frame_str, ft_data.header.frame_id,
        ft_data.header.stamp, ros::Duration(0.1));
    tf_listener->lookupTransform(_sensor_frame_str, ft_data.header.frame_id,
        ft_data.header.stamp, ftdata_to_sensor_tf);

    // Transform forces to the target frame
    _meas_force_sensor_frame = ftdata_to_sensor_tf * tf::Vector3(
        ft_data.wrench.force.x,
        ft_data.wrench.force.y,
        ft_data.wrench.force.z);

    #ifndef IGNORE_TORQUE
      // Transform torques to the target frame
      _meas_torque_sensor_frame = ftdata_to_sensor_tf * tf::Vector3(
          ft_data.wrench.torque.x, 
          ft_data.wrench.torque.y,
          ft_data.wrench.torque.z);
    #endif

    // Filter if filtering is on
    if (_filter_ft_data)
    {
      if (ft_data.header.frame_id == _sensor_frame_str)
        _median_filter();
      else
        _mean_filter();
    }

    if (_got_ft_data == false)
    {
      ROS_INFO("Successfully received the wrench measurements. This message will not be published again.");
      _got_ft_data = true;
    }

    geometry_msgs::WrenchStamped out_msg(ft_data);
    out_msg.header.frame_id = _sensor_frame_str;
    out_msg.wrench.force.x = _meas_force_sensor_frame.x();
    out_msg.wrench.force.y = _meas_force_sensor_frame.y();
    out_msg.wrench.force.z = _meas_force_sensor_frame.z();
    #ifndef IGNORE_TORQUE
      out_msg.wrench.torque.x = _meas_torque_sensor_frame.x();
      out_msg.wrench.torque.y = _meas_torque_sensor_frame.y();
      out_msg.wrench.torque.z = _meas_torque_sensor_frame.z();
    #endif
    return out_msg;
  }
  catch (tf::TransformException ex)
  {
    ROS_ERROR_THROTTLE(3, "Measured F/T data TF lookup failed: %s", ex.what());
    return ft_data;
  }
}

bool WrenchController::CheckInContact(double thresh_force_z)
{
  static int counter = 0;
  if (_target_force_sensor_frame.z() > thresh_force_z)
  {
    if (counter < 1)
      counter = 0;
    counter++;
  }
  else
  {
    if (counter > 4) 
      counter = 4;
    counter--;
  }
  return counter >= 3;
}

bool WrenchController::CalculateThrustTorque(tf::Vector3 &thrust_des, tf::Vector3 &torque_des, const tf::TransformListener *tf_listener)
{
  // Check if we have received all the required data and should publish
  if (!_got_ft_data || !_got_target_wrench)
  {
    if (_got_ft_data == false && _got_target_wrench == false)
      ROS_WARN_THROTTLE(5, "Waiting for the wrench data and desired wrench...");
    else if (_got_ft_data == false)
      ROS_WARN_THROTTLE(5, "Waiting for the wrench data...");
    else
      ROS_WARN_THROTTLE(5, "Waiting for the desired wrench...");
    return false;
  }

  // First, get the transformation of the sensor to robot
  tf::StampedTransform sensor_to_thrust_output_tf, world_to_thrust_output_tf, sensor_to_torque_output_tf;
  try
  {
    ros::Time curr_time = ros::Time::now();

    // Get transform from the sensor to the desired output coordinates
    tf_listener->waitForTransform(_thrust_output_frame_str, _sensor_frame_str,
        curr_time, ros::Duration(0.1));
    tf_listener->lookupTransform(_thrust_output_frame_str, _sensor_frame_str,
        curr_time, sensor_to_thrust_output_tf);
    sensor_to_thrust_output_tf.setOrigin(tf::Vector3(0, 0, 0));
    
    #ifndef IGNORE_TORQUE
      tf_listener->waitForTransform(_torque_output_frame_str, _sensor_frame_str,
          curr_time, ros::Duration(0.1));
      tf_listener->lookupTransform(_torque_output_frame_str, _sensor_frame_str,
          curr_time, sensor_to_torque_output_tf);
      sensor_to_torque_output_tf.setOrigin(tf::Vector3(0, 0, 0));
    #endif

    // Get transform from the world to desired thrust output coordinates
    tf_listener->waitForTransform(_thrust_output_frame_str, _world_frame_str,
        curr_time, ros::Duration(0.1));
    tf_listener->lookupTransform(_thrust_output_frame_str, _world_frame_str,
        curr_time, world_to_thrust_output_tf);

    // Set the target for the force and moment controllers
    _fx_controller->set_target(_target_force_sensor_frame.x());
    _fy_controller->set_target(_target_force_sensor_frame.y());
    _fz_controller->set_target(_target_force_sensor_frame.z());

    #ifndef IGNORE_TORQUE
      _tx_controller->set_target(_target_torque_sensor_frame.x());
      _ty_controller->set_target(_target_torque_sensor_frame.y());
      _tz_controller->set_target(_target_torque_sensor_frame.z());
    #endif

    // Get the desired force and moment rates
    // We directly use these as thrust and torque commands for the robot
    tf::Vector3 delta_thrust_des_sensor_frame = tf::Vector3(
        _fx_controller->get_control(_meas_force_sensor_frame.x(), 0),
        _fy_controller->get_control(_meas_force_sensor_frame.y(), 0),
        _fz_controller->get_control(_meas_force_sensor_frame.z(), 0));

    static tf::Vector3 last_thrust_des(0, 0, 1.0);
    static ros::Time last_time = ros::Time::now();
    auto delta_t = (ros::Time::now() - last_time).toSec();
    last_time = ros::Time::now();
    last_thrust_des = last_thrust_des + delta_thrust_des_sensor_frame * delta_t;
    
    thrust_des = sensor_to_thrust_output_tf * last_thrust_des;


    #ifndef IGNORE_TORQUE
      tf::Vector3 delta_torque_des = sensor_to_torque_output_tf * tf::Vector3(
          _tx_controller->get_control(_meas_torque_sensor_frame.x(), 0),
          _ty_controller->get_control(_meas_torque_sensor_frame.y(), 0),
          _tz_controller->get_control(_meas_torque_sensor_frame.z(), 0));

      static tf::Vector3 last_torque_des(0, 0, 0);
      torque_des = last_torque_des + delta_torque_des;
      last_torque_des = torque_des;
    #endif

    #ifdef DEBUG
      // For debugging
      ROS_ERROR("Current Force: (%0.1lf, %0.1lf, %0.1lf)", _meas_force_sensor_frame.x(), _meas_force_sensor_frame.y(), _meas_force_sensor_frame.z());
      ROS_ERROR("Target Force: (%0.1lf, %0.1lf, %0.1lf)", _target_force_sensor_frame.x(), _target_force_sensor_frame.y(), _target_force_sensor_frame.z());
      ROS_ERROR("Force Loop Out (Sensor Frame): (%0.2lf, %0.2lf, %0.2lf)", delta_thrust_des_sensor_frame.x(), delta_thrust_des_sensor_frame.y(), delta_thrust_des_sensor_frame.z());
      ROS_ERROR("Thrust (World Frame): (%0.2lf, %0.2lf, %0.2lf)", thrust_des.x(), thrust_des.y(), thrust_des.z());
      // ROS_ERROR("Sensor->Thrust:");
      // for (int i = 0; i < 3; ++i)
      //   ROS_ERROR("%7.2lf %7.2lf %7.2lf", sensor_to_thrust_output_tf.getBasis()[i][0],
      //       sensor_to_thrust_output_tf.getBasis()[i][1], sensor_to_thrust_output_tf.getBasis()[i][2]);
      ROS_WARN("------");
    #endif
  }
  catch (tf::TransformException ex)
  {
    ROS_ERROR("Sensor to Robot TF lookup failed: %s", ex.what());
    return false;
  }
  return true;
}

void WrenchController::Reset()
{
  _fx_controller->reset_integral();
  _fy_controller->reset_integral();
  _fz_controller->reset_integral();
  #ifndef IGNORE_TORQUE
    _tx_controller->reset_integral();
    _ty_controller->reset_integral();
    _tz_controller->reset_integral();
  #endif
}

void WrenchController::_median_filter()
{
  // TODO: The filtering can be done more efficient, but for now and considering the size of 5, it should be fine...
  typedef std::tuple<int, tf::Vector3, tf::Vector3> data_tuple;
  const unsigned int max_buffer_size = 5;
  static std::queue<data_tuple> buffer;
  static int seq_counter = 0;
  static std::vector<data_tuple> sorted_list;

  auto new_elem = std::make_tuple(seq_counter++, _meas_force_sensor_frame, _meas_torque_sensor_frame);
  buffer.push(new_elem);
  int new_elem_pos;
  if (buffer.size() > max_buffer_size)
  {
    for (int i = 0; i < sorted_list.size(); ++i)
    {
      if (std::get<0>(sorted_list[i]) == std::get<0>(buffer.front()))
      {
        sorted_list[i] = new_elem;
        new_elem_pos = i;
        break;
      }
    }
    buffer.pop();
  }
  else
  {
    new_elem_pos = sorted_list.size();
    sorted_list.push_back(new_elem);
  }
  
  while (new_elem_pos > 0 && 
    std::get<1>(new_elem).z() < std::get<1>(sorted_list[new_elem_pos - 1]).z())
  {
    std::swap(sorted_list[new_elem_pos], sorted_list[new_elem_pos - 1]);
    new_elem_pos--;
  }

  while (new_elem_pos < sorted_list.size() - 2 && 
    std::get<1>(new_elem).z() > std::get<1>(sorted_list[new_elem_pos + 1]).z())
  {
    std::swap(sorted_list[new_elem_pos], sorted_list[new_elem_pos + 1]);
    new_elem_pos++;
  }

  int median = sorted_list.size() / 2;
  _meas_force_sensor_frame = std::get<1>(sorted_list[median]);
  _meas_torque_sensor_frame = std::get<2>(sorted_list[median]);
}

void WrenchController::_mean_filter()
{
  // Note: This mean filter is pretty much designed with the gazebo force_torque sensor in mind
  // Gazebo's F/T sensor is extremely noisy and frequently drops to near zero while in contact.
  // As a result, I am skipping the zero readings.

  typedef std::pair<tf::Vector3, tf::Vector3> data_pair;

  const double drop_threshold = 1.0f; // in Newtons
  const double time_threshold = 0.3f; // in secs
  const unsigned int max_buffer_size = 9;

  static std::queue<data_pair> buffer;
  static tf::Vector3 force(0, 0, 0), torque(0, 0, 0);
  static ros::Time last_added_time = ros::Time::now();
  
  auto new_elem = data_pair(_meas_force_sensor_frame, _meas_torque_sensor_frame);

  auto curr_time = ros::Time::now();
  if (buffer.empty() || (curr_time - last_added_time).toSec() > time_threshold ||
    std::abs(_meas_force_sensor_frame.z()) > drop_threshold ||
    std::abs(buffer.back().first.z()) <= drop_threshold)
  {
    buffer.push(new_elem);
    last_added_time = curr_time;

    force = force + _meas_force_sensor_frame;
    #ifndef IGNORE_TORQUE
      torque = torque + _meas_torque_sensor_frame;
    #endif

    if (buffer.size() > max_buffer_size)
    {
      force = force - buffer.front().first;
      #ifndef IGNORE_TORQUE
        torque = torque - buffer.front().second;
      #endif
      buffer.pop();
    }
  }

  _meas_force_sensor_frame = force / buffer.size();
  #ifndef IGNORE_TORQUE
    _meas_torque_sensor_frame = torque / buffer.size();
  #endif
}
