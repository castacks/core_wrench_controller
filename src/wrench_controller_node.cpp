#include <wrench_controller/wrench_controller_node.h>
#include <mav_msgs/AttitudeThrust.h>
#include <tuple>

// #define DEBUG
// #define VIRTUAL_FT_SENSOR

WrenchControlNode::WrenchControlNode(std::string node_name)
  : BaseNode(node_name) { }

bool WrenchControlNode::initialize()
{
  ros::NodeHandle* nh = get_node_handle();
  ros::NodeHandle* pnh = get_private_node_handle();

  // Initialize the fence parameters
  GeoFence pos_fence;
  pos_fence.Enabled = pnh->param("geofence/enable", bool(false));
  pos_fence.XMin = pnh->param("geofence/x_min", double(pos_fence.XMin));
  pos_fence.XMax = pnh->param("geofence/x_max", double(pos_fence.XMax));
  pos_fence.YMin = pnh->param("geofence/y_min", double(pos_fence.YMin));
  pos_fence.YMax = pnh->param("geofence/y_max", double(pos_fence.YMax));
  pos_fence.ZMin = pnh->param("geofence/z_min", double(pos_fence.ZMin));
  pos_fence.ZMax = pnh->param("geofence/z_max", double(pos_fence.ZMax));

  // Initialize other parameters
  _target_frame_str = pnh->param("target_frame", std::string("map"));
  _sensor_frame_str = pnh->param("ft_sensor_frame", std::string("ft_sensor"));
  _robot_frame_str = pnh->param("robot_frame", std::string("base_link"));
  _world_frame_str = pnh->param("world_frame", std::string("map"));
  _contact_frame_str = pnh->param("contact_frame", std::string("contact"));

  double xy_vel_limit = pnh->param("max_xy_vel", double(12.f));
  double hover_thrust = pnh->param("hover_thrust", double(0.5f));
  double thrust_max = pnh->param("thrust_max", double(1.0f));
  double thrust_min = pnh->param("thrust_min", double(0.15f));
  double max_tilt_deg = pnh->param("max_tilt", double(45.0f));
  double execute_target = pnh->param("execute_target", double(0.0f));
  bool filter_ft_data = pnh->param("ft_data/filter", bool(true));
  _publish_filtered_ft_data = pnh->param("ft_data/publish", bool(true));

  _pose_controller = new PoseController(pos_fence, _target_frame_str, xy_vel_limit,
      thrust_min, thrust_max, max_tilt_deg, hover_thrust);

  _wrench_controller = new WrenchController(_sensor_frame_str, _robot_frame_str, _target_frame_str,
    _target_frame_str, _robot_frame_str, hover_thrust, filter_ft_data);

  // In an ideal world, these should not be hardcoded and should be determined by the application.
  // For now, let's assume that we touch stuff directly straight in.
  _contact_normal.setValue(-1, 0, 0);
  _velocity_free_matrix.setValue(0, 0, 0, 0, 1, 0, 0, 0, 1);
  _force_constraint_vec.setValue(-1, 0, 0);

  // Initialize the variables
  _should_publish = true;
  _mode_switch = false;

  // Initialize subscribers
  #ifdef VIRTUAL_FT_SENSOR
    _ft_data_sub = nh->subscribe("virtual_ft_data", 10, &WrenchControlNode::_ft_data_callback, this);
  #else
    _ft_data_sub = nh->subscribe("ft_data", 10, &WrenchControlNode::_ft_data_callback, this);
  #endif
  _ft_setpoint_sub = nh->subscribe("ft_setpoint", 10, &WrenchControlNode::_ft_setpoint_callback, this);
  _tracking_point_sub = nh->subscribe("tracking_point", 10, &WrenchControlNode::_tracking_point_callback, this);
  _odometry_sub = nh->subscribe("odometry", 10, &WrenchControlNode::_odometry_callback, this);
  _switch_sub = nh->subscribe("wrench_controller/switch", 10, &WrenchControlNode::_switch_callback, this);
  _tf_listener = new tf::TransformListener();
  _arm_sub = nh->subscribe("arm_active", 10, &WrenchControlNode::_arm_callback, this);

  // Initialize publishers
  _command_pub = nh->advertise<mav_msgs::AttitudeThrust>("attitude_thrust_command", 10);
  _filtered_ft_data_pub = nh->advertise<geometry_msgs::WrenchStamped>("ft_data_filtered", 10);
  #ifdef VIRTUAL_FT_SENSOR
    _virtual_ft_data_pub = nh->advertise<geometry_msgs::WrenchStamped>("virtual_ft_data", 10);
  #endif

  // Initialize services
  _publish_control_server = pnh->advertiseService("publish_control", &WrenchControlNode::_publish_control_callback, this);
  
  // Print some useful information
  ROS_INFO("Subscribed to '%s' for odometry data.", _odometry_sub.getTopic().c_str());
  ROS_INFO("Subscribed to '%s' for wrench measurements data.", _ft_data_sub.getTopic().c_str());
  ROS_INFO("Subscribed to '%s' for pose setpoints.", _tracking_point_sub.getTopic().c_str());
  ROS_INFO("Subscribed to '%s' for wrench setpoints.", _ft_setpoint_sub.getTopic().c_str());

  if (pos_fence.Enabled)
    ROS_INFO("Position geofence is enabled with: x: [%0.1lf, %0.1lf]  y: [%0.1lf, %0.1lf]  z: [%0.1lf, %0.1lf]",
      pos_fence.XMin, pos_fence.XMax, pos_fence.YMin, pos_fence.YMax, pos_fence.ZMin, pos_fence.ZMax);
  else
    ROS_WARN("Geofence is disabled.");

  ROS_INFO("Wrench data filtering is %s.", filter_ft_data ? "on" : "off");

  if (_mode_switch == 0)
    ROS_INFO("Pose control mode active: Only controlling the pose.");
  else
    ROS_INFO("Motion-Force control mode active: Controlling both motion and force.");

  ROS_INFO("Controller loop running with %0.1lf Hz.", execute_target);

  return true;
}

bool WrenchControlNode::execute()
{

  tf::Vector3 thrust_pose_des, thrust_wrench_des, torque_wrench_des;
  if (_pose_controller->CalculateThrust(thrust_pose_des) == false)
    return true;
  
  // static bool contact_status = false;
  // // Check if the contact status has changed
  // bool is_in_contact = _wrench_controller->CheckInContact(5.0f);
  // if (contact_status == false && is_in_contact == true)
  //   ROS_INFO("Switching to contact mode.");
  // else if (contact_status == true && is_in_contact == false)
  //   ROS_INFO("Switching to position mode.");
  // contact_status = is_in_contact;

  tf::Vector3 thrust_des;
  if (_mode_switch)
  {
    if (_wrench_controller->CalculateThrustTorque(thrust_wrench_des, torque_wrench_des, _tf_listener) == false) 
      return true;
    // Mix the thrusts from the pose and wrench
    if (CombineMotionAndForce(thrust_wrench_des, thrust_pose_des, _contact_normal, _velocity_free_matrix, 
      _force_constraint_vec, _target_frame_str, thrust_des, ros::Time::now()) == false)
      return true; // Return if an exception has happened
  }
  else
    thrust_des = thrust_pose_des;
  
  // Calculate the desired attitude from the thrust
  tf::Quaternion att_sp;
  double total_thrust;
  std::tie(att_sp, total_thrust) = _pose_controller->CalculateAttitudeThrust(thrust_des);

  // Publish the desired thrusts
  mav_msgs::AttitudeThrust drone_cmd;
  drone_cmd.attitude.x = att_sp.getX();
  drone_cmd.attitude.y = att_sp.getY();
  drone_cmd.attitude.z = att_sp.getZ();
  drone_cmd.attitude.w = att_sp.getW();
  drone_cmd.thrust.x = 0;
  drone_cmd.thrust.y = 0;
  drone_cmd.thrust.z = total_thrust;

  _command_pub.publish(drone_cmd);

  // Publish the virtual F/T sensor data
  #ifdef VIRTUAL_FT_SENSOR
    // Get transform from the target wrench to sensor coordinates
    ros::Time curr_time = ros::Time::now();
    tf::StampedTransform target_to_sensor_tf;
    try
    {
      _tf_listener->waitForTransform(_sensor_frame_str, _target_frame_str,
          curr_time, ros::Duration(0.1));
      _tf_listener->lookupTransform(_sensor_frame_str, _target_frame_str,
          curr_time, target_to_sensor_tf);
          target_to_sensor_tf.setOrigin(tf::Vector3(0, 0, 0));

      // Transform forces to sensor frame
      tf::Vector3 virtual_force = target_to_sensor_tf * tf::Vector3(
        std::max(0.0, -thrust_des.x() * _contact_normal.x()),
        std::max(0.0, -thrust_des.y() * _contact_normal.y()),
        std::max(0.0, -thrust_des.z() * _contact_normal.z()));
      
      geometry_msgs::WrenchStamped virtual_ft_data;
      double scaling = 30.0f;
      virtual_ft_data.header.frame_id = _sensor_frame_str;
      virtual_ft_data.header.stamp = curr_time;
      virtual_ft_data.wrench.force.x = virtual_force.x() * scaling;
      virtual_ft_data.wrench.force.y = virtual_force.y() * scaling;
      virtual_ft_data.wrench.force.z = virtual_force.z() * scaling;
      _virtual_ft_data_pub.publish(virtual_ft_data);
    }
    catch (tf::TransformException ex) {}
  #endif

  
  return true;
}

bool WrenchControlNode::CombineMotionAndForce(const tf::Vector3 &thrust_force,
  const tf::Vector3 &thrust_motion, tf::Vector3 contact_normal, const tf::Matrix3x3 &vel_mat, 
  const tf::Vector3 &force_constraint_vec, const std::string &thrust_frame, tf::Vector3 &out_thrust_des, 
  const ros::Time &query_time)
{
  try
  {
    // First, check if we are still supposed to apply force or we should
    // fly away from the contact
    if (thrust_motion.dot(contact_normal) > 0)
    {
      out_thrust_des = thrust_motion;
      #ifdef DEBUG
        // For debugging
        ROS_ERROR("Thrust From Force: (%0.2lf, %0.2lf, %0.2lf)", thrust_force.x(), thrust_force.y(), thrust_force.z());
        ROS_ERROR("Thrust From Motion: (%0.2lf, %0.2lf, %0.2lf)", thrust_motion.x(), thrust_motion.y(), thrust_motion.z());
        ROS_ERROR("Final thrust is only from the motion. Heading away from the wall.");
        ROS_WARN("------");
      #endif
      return true;
    }
    
    // Get the transform from the thrust's frame (probably world) to contact
    tf::StampedTransform tf_cv;
    _tf_listener->waitForTransform(_contact_frame_str, thrust_frame, query_time, ros::Duration(0.1));
    _tf_listener->lookupTransform(_contact_frame_str, thrust_frame, query_time, tf_cv);

    // Convert the thrusts to the contact frame
    tf::Vector3 thrust_force_c = tf_cv * thrust_force;
    tf::Vector3 thrust_motion_c = tf_cv * thrust_motion;

    // Get the force constraint matrix
    tf::Matrix3x3 force_mat(1 - vel_mat[0][0], 0, 0, 0, 1 - vel_mat[1][1], 0, 0, 0, 1 - vel_mat[2][2]);

    // Apply the constraints to the force
    tf::Vector3 thrust_force_constrained_c = _apply_contact_constraints(thrust_force_c, contact_normal, 
      force_mat, force_constraint_vec);

    // Apply the constraints to the motion
    tf::Vector3 thrust_motion_constrained_c = _apply_contact_constraints(thrust_motion_c, contact_normal, 
      vel_mat, -force_constraint_vec);

    // Combine the thrusts and transform back to the thrust frames
    out_thrust_des = tf_cv.inverse() * (thrust_force_constrained_c + thrust_motion_constrained_c);

    #ifdef DEBUG
      // For debugging
      ROS_ERROR("Thrust From Force: (%0.2lf, %0.2lf, %0.2lf)", thrust_force.x(), thrust_force.y(), thrust_force.z());
      ROS_ERROR("Thrust From Motion: (%0.2lf, %0.2lf, %0.2lf)", thrust_motion.x(), thrust_motion.y(), thrust_motion.z());
      ROS_ERROR("Initial Mixed Thrust: (%0.2lf, %0.2lf, %0.2lf)", out_thrust_des.x(), out_thrust_des.y(), out_thrust_des.z());
    #endif

    tf::Vector3 thrust_h_des = _pose_controller->ConstrainHorizontalThrust(out_thrust_des);
    out_thrust_des.setX(thrust_h_des.x());
    out_thrust_des.setY(thrust_h_des.y());

    #ifdef DEBUG
      // For debugging
      ROS_ERROR("Final Mixed Thrust: (%0.2lf, %0.2lf, %0.2lf)", out_thrust_des.x(), out_thrust_des.y(), out_thrust_des.z());
      ROS_WARN("------");
    #endif

    return true;
  }
  catch(const std::exception& ex)
  {
    ROS_ERROR("Thrust to Contact TF lookup failed: %s", ex.what());
    return false;
  }  
}

tf::Vector3 WrenchControlNode::_apply_contact_constraints(const tf::Vector3 &vec_c, 
  const tf::Vector3 &contact_normal, const tf::Matrix3x3 &free_mat, const tf::Vector3 &constraint_vec)
{
  // In the constraint vector, for each element of input vector in the contact frame we apply: 
  // 0: no constraint, 1: can only be positive, -1: can only be negative

  // The free vector applied, which only keeps the free-to-move directions and zeros the rest
  tf:: Vector3 vec_free_c = free_mat * vec_c;

  // Apply the constraint matrix
  for (int i = 0; i < 3; ++i)
    if (vec_c[i] * constraint_vec[i] < 0)
      vec_free_c[i] = 0;

  return vec_free_c;
}

void WrenchControlNode::_ft_data_callback(const geometry_msgs::WrenchStamped &wrench)
{
  geometry_msgs::WrenchStamped out_data = _wrench_controller->UpdateState(wrench, _tf_listener);
  if (_publish_filtered_ft_data)
    _filtered_ft_data_pub.publish(out_data);
}

void WrenchControlNode::_arm_callback(const std_msgs::Bool &armed)
{
  if(armed.data == true)
  {
    ROS_ERROR("I'm reseting now!");
    _pose_controller->Reset();
  }
}

void WrenchControlNode::_ft_setpoint_callback(const geometry_msgs::WrenchStamped &wrench)
{
  _wrench_controller->UpdateTarget(wrench, _tf_listener);
}

void WrenchControlNode::_tracking_point_callback(const nav_msgs::Odometry &tracking_point)
{
  _pose_controller->UpdateTarget(tracking_point, _tf_listener);
}

void WrenchControlNode::_odometry_callback(const nav_msgs::Odometry &odom)
{
  _pose_controller->UpdateState(odom, _tf_listener);
}

void WrenchControlNode::_switch_callback(const std_msgs::Bool &mode_switch)
{
  _mode_switch = mode_switch.data;
  if (_mode_switch == 0)
    ROS_INFO("Switching to pose control mode.");
  else
    ROS_INFO("Switching to motion-force control mode.");
}

bool WrenchControlNode::_publish_control_callback(std_srvs::SetBool::Request& request, std_srvs::SetBool::Response& response)
{
  // Reset the PID integrators when just starting to publish
  if (!_should_publish && request.data)
    _wrench_controller->Reset();
  
  _should_publish = request.data;
  response.success = true;
  return true;
}

WrenchControlNode::~WrenchControlNode() { }

BaseNode* BaseNode::get()
{
  WrenchControlNode* wrench_control_node = new WrenchControlNode("wrench_controller");
  return wrench_control_node;
}
