#ifndef _WRENCH_CONTROLLER_NODE_H_
#define _WRENCH_CONTROLLER_NODE_H_

#include <ros/ros.h>
#include <tf/transform_listener.h>
#include <base/BaseNode.h>
#include <nav_msgs/Odometry.h>
#include <std_srvs/SetBool.h>
#include <std_msgs/Bool.h>
#include <geometry_msgs/WrenchStamped.h>
#include <pose_controller/pose_controller.h>
#include <wrench_controller/wrench_controller.h>

class WrenchControlNode : public BaseNode 
{
private:

  // Subscribers
  ros::Subscriber _tracking_point_sub, _odometry_sub, _ft_data_sub, _ft_setpoint_sub, _switch_sub, _arm_sub;

  // Publishers
  ros::Publisher _command_pub, _filtered_ft_data_pub, _virtual_ft_data_pub;

  // Services
  ros::ServiceServer _publish_control_server;

  // TF Listeners
  tf::TransformListener* _tf_listener;

  // Variables
  PoseController* _pose_controller;
  WrenchController* _wrench_controller;
  bool _should_publish, _mode_switch, _publish_filtered_ft_data;
  tf::Vector3 _contact_normal, _force_constraint_vec;
  tf::Matrix3x3 _velocity_free_matrix;
  
  // Coordinate frame names
  std::string _world_frame_str, _robot_frame_str, _sensor_frame_str, _contact_frame_str, _target_frame_str;
  
  // Callback functions
  void _ft_data_callback(const geometry_msgs::WrenchStamped &wrench);
  void _ft_setpoint_callback(const geometry_msgs::WrenchStamped &wrench);
  void _tracking_point_callback(const nav_msgs::Odometry &odom);
  void _odometry_callback(const nav_msgs::Odometry &odom);
  bool _publish_control_callback(std_srvs::SetBool::Request& request, std_srvs::SetBool::Response& response);
  void _switch_callback(const std_msgs::Bool &mode_switch);
  void _arm_callback(const std_msgs::Bool &armed);
  
  // Helper functions
  tf::Vector3 _apply_contact_constraints(const tf::Vector3 &vec, const tf::Vector3 &contact_normal, 
    const tf::Matrix3x3 &free_mat, const tf::Vector3 &constraint_vec);

public:
  WrenchControlNode(std::string node_name);
  virtual bool initialize();
  virtual bool execute();
  virtual ~WrenchControlNode();
  bool CombineMotionAndForce(const tf::Vector3 &thrust_force, const tf::Vector3 &thrust_motion, 
    tf::Vector3 contact_normal, const tf::Matrix3x3 &vel_mat, const tf::Vector3 &force_constraint_vec, 
    const std::string &vec_frame, tf::Vector3 &out_thrust_des, const ros::Time &query_time);
};

#endif
