#ifndef _WRENCH_CONTROLLER_H_
#define _WRENCH_CONTROLLER_H_

#include <core_pid_controller/pid_controller.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/WrenchStamped.h>
#include <tf/transform_listener.h>
#include <tuple>

class WrenchController
{
private:
  // Controllers
  PIDController* _fx_controller;
  PIDController* _fy_controller;
  PIDController* _fz_controller;
  PIDController* _tx_controller;
  PIDController* _ty_controller;
  PIDController* _tz_controller;

  // Variables
  std::string _sensor_frame_str, _robot_frame_str, _world_frame_str, _thrust_output_frame_str, _torque_output_frame_str;
  double _last_thrust, _hover_thrust;
  bool _got_target_wrench, _got_ft_data, _filter_ft_data;
  
  // Latest target wrench and the measured force/torque sensor data
  geometry_msgs::WrenchStamped _target_wrench, _current_wrench;
  tf::Vector3 _target_force_sensor_frame, _target_torque_sensor_frame;
  tf::Vector3 _meas_force_sensor_frame, _meas_torque_sensor_frame;

  // Helper functions
  void _median_filter();
  void _mean_filter();

public:
  WrenchController(const std::string &sensor_frame, const std::string &robot_frame, const std::string &world_frame, 
    const std::string &thrust_output_frame, const std::string &torque_output_frame, double hover_thrust, bool filter_ft_data);
  void UpdateTarget(const geometry_msgs::WrenchStamped &target, const tf::TransformListener *tf_listener);
  geometry_msgs::WrenchStamped UpdateState(const geometry_msgs::WrenchStamped &ft_data, const tf::TransformListener *tf_listener);
  bool CalculateThrustTorque(tf::Vector3 &thrust_des, tf::Vector3 &torque_des, const tf::TransformListener *tf_listener);
  bool CheckInContact(double thresh_force_z);
  void Reset();
};

#endif
